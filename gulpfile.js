'use strict';
// Load all depencies
var gulp  = require('gulp'),

// Notify when a task is run and completed
    notify = require("gulp-notify"),

// For styles task
    sass  = require('gulp-sass'),
    pixrem = require('gulp-pixrem'),
    autoprefixer  = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),

// For scripts task
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),

// For images task
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant')

// Tasks

// Sass task
gulp.task('scss', function() {
  gulp.src([
      './scss/*.scss',
      '!./scss/_*.scss'
      ])
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 4 versions', 'Safari 5', 'ie 10-11', 'Opera 12.1','iOS 6', 'Android 4']
    }))
    .pipe(pixrem({
      rootValue: '16px',
      atrules: true // atrules: true enables pixel fallbacks for rems specified inside media queries.
    }))
    .pipe(sourcemaps.write('maps', {
      includeContent: false,
      sourceRoot: 'stylesheets/'
    }))
    .pipe(gulp.dest('./stylesheets'))
    .pipe(notify({
      title: 'Sass:',
      message: 'Compiled <%= file.relative %>'
    }));
});

// Concat and rename designated scripts into one file
gulp.task('scripts', function() {
   return gulp.src(['./javascripts/vendor/browser-update.js'])
     .pipe(concat('all.js'))
     .pipe(uglify())
     .pipe(rename('all-min.js'))
     .pipe(gulp.dest('./javascripts/'));
});

// Compress Image Task
gulp.task('images', function(){
  gulp.src('images/**/*')
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('images/'));
});


// Use Sass and watch in Default Task & watch appropriate files
gulp.task('default', ['scss', 'scripts'], function(){
  gulp.watch([  'scss/**/*.scss'], ['scss']);
  gulp.watch([  'js/**/*.js'], ['scripts']);
});
