# Portfolio of Jason Markle

**Description:** A portfolio, about, and sandbox for web things and stuff.

**Developer:** Jason Markle

**Repositoy URL:**
[https://github.com/jasonmarkle/jasonmarkle.github.io](https://github.com/jasonmarkle/jasonmarkle.github.io)

**Dependencies:**
Sass

**Lisence:**
This project is licensed under the MIT License - see the [LICENSE.md](https://github.com/jasonmarkle/portfolio/blob/master/LICENSE) file for details
